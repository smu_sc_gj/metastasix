﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class CellSelection : MonoBehaviour
{

    public Grid grid;
    public Vector3Int Level1Cell;
    public Vector3Int exitCell;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxis("Pick") > 0.0f)
        {
            //Debug.Log("Pick is down, did I cast?");
            Vector3Int chosenCell = ClickSelect();
            Vector3Int chosenCell1Up = chosenCell;
            Vector3Int chosenCell2Up = chosenCell;
            chosenCell1Up.z = 1;
            chosenCell2Up.z = 2;

            //Debug.Log("Cell coordinates" + chosenCell);

            Component[] tilemaps = grid.GetComponentsInChildren(typeof(Tilemap));

            if(tilemaps != null)
            {
                foreach(Tilemap map in tilemaps)
                {
                    //Debug.Log("Tiles in map " + map.GetUsedTilesCount());
                    if(map.HasTile(chosenCell) == true)
                    {
                        TileBase tile = map.GetTile(chosenCell);
                        Debug.Log("Tile is " + tile.name);
                        Debug.Log("Tile is at " + chosenCell );

                        if(chosenCell == Level1Cell)
                            SceneManager.LoadScene("Level1");
                    }
                    else
                    {
                        
                        //Debug.Log("No tile here");
                    }

                    if(map.HasTile(chosenCell1Up) == true)
                    {
                        TileBase tile = map.GetTile(chosenCell1Up);
                        Debug.Log("Tile is " + tile.name);
                        Debug.Log("Tile is at " + chosenCell1Up );

                        if(chosenCell1Up == exitCell)
                            Application.Quit();
                    }
                    else
                    {
                        
                        //Debug.Log("No tile here");
                    }

                    if(map.HasTile(chosenCell2Up) == true)
                    {
                        TileBase tile = map.GetTile(chosenCell2Up);
                        Debug.Log("Tile is " + tile.name);
                        Debug.Log("Tile is at " + chosenCell2Up );

                        if(chosenCell2Up == exitCell)
                            Application.Quit();
                    }
                    else
                    {
                        
                        //Debug.Log("No tile here");
                    }
                }
            }
            else
            {
                Debug.Log("No tilemaps found");
            }
        }
    }

    // Based on this thread - https://forum.unity.com/threads/2017-tilemap-system-select-tile-with-mouse-ingame.506249/
    // https://blogs.unity3d.com/2018/05/29/procedural-patterns-you-can-use-with-tilemaps-part-i/
    Vector3Int ClickSelect()
     {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
        Vector3Int coordinate = grid.WorldToCell(mouseWorldPos);
        //Debug.Log(coordinate);

        return coordinate;
     }
}
