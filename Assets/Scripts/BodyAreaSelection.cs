﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class BodyAreaSelection : MonoBehaviour
{
    public Vector3Int Level1Cell = new Vector3Int(4,1,0); 
    public float radius = 5; // 
    public Vector3Int exitCell = new Vector3Int(10,-2,0);
    public string level1Name = "Level1";

    Grid grid;

    // Start is called before the first frame update
    void Start()
    {
       grid = GetComponent<Grid>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxis("Pick") > 0.0f)
        {
            //Debug.Log("Pick is down, did I cast?");
            Vector3Int chosenCell = ClickSelect();
            Vector3Int chosenCell1Up = chosenCell;
            Vector3Int chosenCell2Up = chosenCell;
            chosenCell1Up.z = 1;
            chosenCell2Up.z = 2;

            //Debug.Log("Cell coordinates" + chosenCell);

            Component[] tilemaps = grid.GetComponentsInChildren(typeof(Tilemap));

            if(tilemaps != null)
            {
                foreach(Tilemap map in tilemaps)
                {
                    //Debug.Log("Tiles in map " + map.GetUsedTilesCount());
                    if(map.HasTile(chosenCell) == true)
                    {
                        TileBase tile = map.GetTile(chosenCell);
                        Debug.Log("Tile is " + tile.name);
                        Debug.Log("Tile is at " + chosenCell );

                        if(Vector3.Distance(chosenCell,Level1Cell) < radius)
                            SceneManager.LoadScene(level1Name);
                    }
                    else
                    {
                        
                        //Debug.Log("No tile here");
                    }
                }
            }
            else
            {
                Debug.Log("No tilemaps found");
            }
        }
    }

    // Based on this thread - https://forum.unity.com/threads/2017-tilemap-system-select-tile-with-mouse-ingame.506249/
    // https://blogs.unity3d.com/2018/05/29/procedural-patterns-you-can-use-with-tilemaps-part-i/
    Vector3Int ClickSelect()
     {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
        Vector3Int coordinate = grid.WorldToCell(mouseWorldPos);
        //Debug.Log(coordinate);

        return coordinate;
     }
}
