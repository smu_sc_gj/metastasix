# READ ME #

Metastix is a game designed to teach people about cancer. 

**Unity Version: 2020.1.4f1**

## Getting the Unity3D Project ##

## Cloning the repository (commandline) ##

Only use this if you are familiar with using git on the commandline! 

First make sure you have either a suitable git package or [GitBash](https://gitforwindows.org/) installed, you'll also need [Git LFS (Large File Storage)](https://git-lfs.github.com/), you'll also need ot make sure it's installed before cloning. 

```bash
git lfs install
```

Then create a local clone. 

```bash
git clone https://smu_sc_gj@bitbucket.org/smu_sc_gj/metastasix.git
```

Altassian provide some [handy guides](see [Clone a repository](https://confluence.atlassian.com/x/4whODQ) if you're unsure. 

## Clone a repository (without using the command line) ##

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

# CREDIT #

Artwork from the Unity Isometric Tiles tutorial were created by [castpixel](https://twitter.com/castpixel) and you can view her Portfolio [here](http://castpixel.tumblr.com/) use in the test level only. 

2D PixelArt - Isometric Blocks - [Devil's Work.shop](https://assetstore.unity.com/packages/3d/environments/2d-pixelart-isometric-blocks-115039) use under Standard Unity Asset Store EULA. 

# Notes #
1. Created a scene called Isometric_Test to try out the isometric tile tools and some of the scripts required for camera control, picking etc. 
  * Sprites are from the Isometric (Assets/Isometric) tiles tutorial - probably can't use these. 
  * Working though these [tutorials](https://www.gamedev.net/tutorials/programming/general-and-gameplay-programming/unity-isometric-tilemap-tutorials-r5509/) currently stealing ideas from 3. 
  * **NOTE** getting the sprites setup for Unity is not trivial, need to set pivot points once they are imported. The tutorial doesn't explain this very well, I found it easier to add them to a palet first then work out the right pivot point for them. 
2. This would be a better way of creating the cells https://blogs.unity3d.com/2018/06/07/procedural-patterns-to-use-with-tilemaps-part-i/ https://blogs.unity3d.com/2018/06/07/procedural-patterns-to-use-with-tilemaps-part-ii/
3. Created a whole body screen, to choose the body part, at the moment there's a hot spot located in the region of the lungs. The toe of the left foot should exit the game ...
